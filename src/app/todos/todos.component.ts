import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todos$:Observable<any>;
  todo:String;

  constructor(public mainService:MainService) { }


  ngOnInit() {
    this.todos$ = this.mainService.getTodos();
    
  }
 
}

