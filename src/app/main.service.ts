import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Main } from './interfaces/main';
import { Photos } from './interfaces/photos';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  apiUrl='https://jsonplaceholder.typicode.com/posts/';
  photoUrl= 'https://jsonplaceholder.typicode.com/todos';

  constructor(private http: HttpClient, private db: AngularFirestore, private router:Router) { }
  getMains(){
    return this.http.get<Main[]>(this.apiUrl)
  }
  getPhotos(){
    return this.http.get<Photos[]>(this.photoUrl)
  }
   
  getTodos():Observable<any[]>{
    return this.db.collection('todos').valueChanges({idField:'id'});
     }

  addTodo(title:String, completed:boolean){
    const todo = {title:title, completed:completed}
    this.db.collection('todos').add(todo) 
    .then(
    res=>
    { 
    this.router.navigate(['/todos']);
    }
    )
}
}
