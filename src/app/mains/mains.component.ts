import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Main } from '../interfaces/main';
import { MainService } from '../main.service';
import { Photos } from '../interfaces/photos';

@Component({
  selector: 'app-mains',
  templateUrl: './mains.component.html',
  styleUrls: ['./mains.component.css']
})
export class MainsComponent implements OnInit {
  //geting posts from json not from db
  mains$: Main[];
  photos$: Photos[];

  

  constructor(private mainService:MainService) { }

  mySave(title:string, completed:boolean){
    this.mainService.addTodo(title,completed);
 }

  ngOnInit() {
     this.mainService.getPhotos().subscribe(data => this.photos$ = data);
    this.mainService.getMains().subscribe(data => this.mains$ = data);
  }
  

}
